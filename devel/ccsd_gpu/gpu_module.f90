
module gpu_module
  use iso_c_binding
  implicit none

  interface
    type(c_ptr) function gpu_init(nO, nV, cholesky_mo_num, &
      cc_space_v_oo_chol, cc_space_v_ov_chol, cc_space_v_vo_chol, cc_space_v_vv_chol, &
      cc_space_v_oooo, cc_space_v_vooo, cc_space_v_voov, cc_space_v_oovv, cc_space_v_vvoo, &
      cc_space_v_oovo, cc_space_v_ovvo, cc_space_v_ovov, cc_space_v_ovoo, &
      cc_space_f_oo, cc_space_f_ov, cc_space_f_vo, cc_space_f_vv) bind(C)
        import c_int, c_double, c_ptr
        integer(c_int), intent(in), value :: nO, nV, cholesky_mo_num
        real(c_double), intent(in)  :: cc_space_v_oo_chol(cholesky_mo_num,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_ov_chol(cholesky_mo_num,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_vo_chol(cholesky_mo_num,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_vv_chol(cholesky_mo_num,nV,nV)
        real(c_double), intent(in)  :: cc_space_v_oooo(nO,nO,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_vooo(nV,nO,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_voov(nV,nO,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_oovv(nO,nO,nV,nV)
        real(c_double), intent(in)  :: cc_space_v_vvoo(nV,nV,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_oovo(nO,nO,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_ovvo(nO,nV,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_ovov(nO,nV,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_ovoo(nO,nV,nO,nO)
        real(c_double), intent(in)  :: cc_space_f_oo(nO,nO)
        real(c_double), intent(in)  :: cc_space_f_ov(nO,nV)
        real(c_double), intent(in)  :: cc_space_f_vo(nV,nO)
        real(c_double), intent(in)  :: cc_space_f_vv(nV,nV)
    end function

    type(c_ptr) function gpu_init_sp(nO, nV, cholesky_mo_num, &
      cc_space_v_oo_chol, cc_space_v_ov_chol, cc_space_v_vo_chol, cc_space_v_vv_chol, &
      cc_space_v_oooo, cc_space_v_vooo, cc_space_v_voov, cc_space_v_oovv, cc_space_v_vvoo, &
      cc_space_v_oovo, cc_space_v_ovvo, cc_space_v_ovov, cc_space_v_ovoo, &
      cc_space_f_oo, cc_space_f_ov, cc_space_f_vo, cc_space_f_vv) bind(C)
        import c_int, c_double, c_ptr
        integer(c_int), intent(in), value :: nO, nV, cholesky_mo_num
        real(c_double), intent(in)  :: cc_space_v_oo_chol(cholesky_mo_num,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_ov_chol(cholesky_mo_num,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_vo_chol(cholesky_mo_num,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_vv_chol(cholesky_mo_num,nV,nV)
        real(c_double), intent(in)  :: cc_space_v_oooo(nO,nO,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_vooo(nV,nO,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_voov(nV,nO,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_oovv(nO,nO,nV,nV)
        real(c_double), intent(in)  :: cc_space_v_vvoo(nV,nV,nO,nO)
        real(c_double), intent(in)  :: cc_space_v_oovo(nO,nO,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_ovvo(nO,nV,nV,nO)
        real(c_double), intent(in)  :: cc_space_v_ovov(nO,nV,nO,nV)
        real(c_double), intent(in)  :: cc_space_v_ovoo(nO,nV,nO,nO)
        real(c_double), intent(in)  :: cc_space_f_oo(nO,nO)
        real(c_double), intent(in)  :: cc_space_f_ov(nO,nV)
        real(c_double), intent(in)  :: cc_space_f_vo(nV,nO)
        real(c_double), intent(in)  :: cc_space_f_vv(nV,nV)
    end function

    subroutine gpu_upload(gpu_data, nO, nV, t1, t2) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in) :: t1(nO,nV)
        real(c_double), intent(in) :: t2(nO,nO,nV,nV)
    end subroutine

    subroutine gpu_upload_sp(gpu_data, nO, nV, t1, t2) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in) :: t1(nO,nV)
        real(c_double), intent(in) :: t2(nO,nO,nV,nV)
    end subroutine


    subroutine compute_H_oo_chol_gpu(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_H_vo_chol_gpu(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_H_vv_chol_gpu(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_r1_space_chol_gpu(gpu_data, nO, nV, t1, r1, max_r1) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in)  :: t1(nO,nV)
        real(c_double), intent(out) :: r1(nO,nO,nV,nV)
        real(c_double), intent(out) :: max_r1
    end subroutine

    subroutine compute_r2_space_chol_gpu(gpu_data, nO, nV, t1, r2, max_r2) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in)  :: t1(nO,nV)
        real(c_double), intent(out) :: r2(nO,nO,nV,nV)
        real(c_double), intent(out) :: max_r2
    end subroutine

    double precision function ccsd_energy_space_gpu(gpu_data) bind(C)
        import c_ptr
        type(c_ptr), value    :: gpu_data
    end function


    subroutine compute_H_oo_chol_gpu_sp(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_H_vo_chol_gpu_sp(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_H_vv_chol_gpu_sp(gpu_data, igpu) bind(C)
        import c_int, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: igpu
    end subroutine

    subroutine compute_r1_space_chol_gpu_sp(gpu_data, nO, nV, t1, r1, max_r1) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in)  :: t1(nO,nV)
        real(c_double), intent(out) :: r1(nO,nO,nV,nV)
        real(c_double), intent(out) :: max_r1
    end subroutine

    subroutine compute_r2_space_chol_gpu_sp(gpu_data, nO, nV, t1, r2, max_r2) bind(C)
        import c_int, c_double, c_ptr
        type(c_ptr), value    :: gpu_data
        integer(c_int), intent(in), value  :: nO, nV
        real(c_double), intent(in)  :: t1(nO,nV)
        real(c_double), intent(out) :: r2(nO,nO,nV,nV)
        real(c_double), intent(out) :: max_r2
    end subroutine

    double precision function ccsd_energy_space_gpu_sp(gpu_data) bind(C)
        import c_ptr
        type(c_ptr), value    :: gpu_data
    end function

    subroutine gpu_dgemm(transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) bind(C)
      import c_int, c_double, c_char
      character(c_char), value :: transa, transb
      integer(c_int), value :: m,n,k,lda,ldb,ldc
      real(c_double), value :: alpha, beta
      real(c_double)        :: A(lda,*), B(ldb,*), C(ldc,*)
    end subroutine

  end interface

end module
