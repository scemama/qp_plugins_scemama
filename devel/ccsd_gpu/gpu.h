#define MULTIGPU 1

typedef struct {
   double* cc_space_v_oo_chol;
   double* cc_space_v_ov_chol;
   double* cc_space_v_vo_chol;
   double* cc_space_v_vv_chol;
   double* cc_space_v_oooo;
   double* cc_space_v_vooo;
   double* cc_space_v_voov;
   double* cc_space_v_oovv;
   double* cc_space_v_vvoo;
   double* cc_space_v_oovo;
   double* cc_space_v_ovvo;
   double* cc_space_v_ovov;
   double* cc_space_v_ovoo;
   double* cc_space_f_oo;
   double* cc_space_f_ov;
   double* cc_space_f_vo;
   double* cc_space_f_vv;
   double* tau;
   double* tau_x;
   double* t1;
   double* t2;
   double* H_oo;
   double* H_vo;
   double* H_vv;
   int nO;
   int nV;
   int cholesky_mo_num;
} gpu_data;

typedef struct {
   float* cc_space_v_oo_chol;
   float* cc_space_v_ov_chol;
   float* cc_space_v_vo_chol;
   float* cc_space_v_vv_chol;
   float* cc_space_v_oooo;
   float* cc_space_v_vooo;
   float* cc_space_v_voov;
   float* cc_space_v_oovv;
   float* cc_space_v_vvoo;
   float* cc_space_v_oovo;
   float* cc_space_v_ovvo;
   float* cc_space_v_ovov;
   float* cc_space_v_ovoo;
   float* cc_space_f_oo;
   float* cc_space_f_ov;
   float* cc_space_f_vo;
   float* cc_space_f_vv;
   float* tau;
   float* tau_x;
   float* t1;
   float* t2;
   float* H_oo;
   float* H_vo;
   float* H_vv;
   int nO;
   int nV;
   int cholesky_mo_num;
} gpu_data_sp;


static cudaError_t gpu_malloc(void** ptr, size_t size) {
    size_t free, total;
    cudaError_t rc = cudaMemGetInfo( &free, &total );
    if (rc != cudaSuccess) return rc;

    if (size < free && size < total/10) {
      rc= cudaMalloc(ptr, size);
    } else {
      rc = cudaMallocManaged(ptr, size, cudaMemAttachGlobal);
    }
    return rc;
}
