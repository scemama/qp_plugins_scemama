program cassdtq
  implicit none
  BEGIN_DOC
  ! Selected CISDTQ with stochastic selection and PT2.
  END_DOC

  if (.not.is_zmq_slave) then
    PROVIDE psi_det psi_coef mo_two_e_integrals_in_map

    if (do_pt2) then
      call run_stochastic_cipsi
    else
      call run_cipsi
    endif

  else
    PROVIDE mo_two_e_integrals_in_map

    call run_slave_cipsi

  endif
end
