use bitmasks

logical function is_a_generator(det)
  use bitmasks
  implicit none
  integer(bit_kind), intent(in)  :: det(N_int,2)
  integer :: na, nb
  integer, external :: number_of_holes, number_of_particles
  provide N_int
  na = number_of_holes(det)
  nb = number_of_particles(det)
  is_a_generator = (na <= 2) .and. (nb <= 2)
end

BEGIN_PROVIDER [ integer, N_det_generators ]
  implicit none
  BEGIN_DOC
  ! Number of generator detetrminants
  END_DOC
  integer                        :: i,k,l
  logical, external              :: is_a_generator
  provide N_int
  call write_time(6)
  N_det_generators = 0
  do i=1,N_det
    if (is_a_generator(psi_det_sorted(1,1,i))) then
      N_det_generators += 1
    endif
  enddo
  N_det_generators = max(N_det_generators,1)
  call write_int(6,N_det_generators,'Number of generators')
END_PROVIDER

 BEGIN_PROVIDER [ integer(bit_kind), psi_det_generators, (N_int,2,N_det) ]
&BEGIN_PROVIDER [ double precision, psi_coef_generators, (N_det,N_states) ]
&BEGIN_PROVIDER [ integer(bit_kind), psi_det_sorted_gen, (N_int,2,N_det) ]
&BEGIN_PROVIDER [ double precision, psi_coef_sorted_gen, (N_det,N_states) ]
&BEGIN_PROVIDER [ integer, psi_det_sorted_gen_order, (N_det) ]
  implicit none
  BEGIN_DOC
  ! For Single reference wave functions, the generator is the
  ! Hartree-Fock determinant
  END_DOC
  integer                        :: i, k, l, m
  logical, external              :: is_a_generator
  integer, allocatable           :: nongen(:)
  integer                        :: inongen

  allocate(nongen(N_det))

  inongen = 0
  m=0
  do i=1,N_det
    if (is_a_generator(psi_det_sorted(1,1,i))) then
      m = m+1
      psi_det_sorted_gen_order(i) = m
      do k=1,N_int
        psi_det_generators(k,1,m) = psi_det_sorted(k,1,i)
        psi_det_generators(k,2,m) = psi_det_sorted(k,2,i)
      enddo
      psi_coef_generators(m,:) = psi_coef_sorted(i,:)
    else
      inongen += 1
      nongen(inongen) = i
    endif
  enddo

  psi_det_sorted_gen(:,:,:N_det_generators) = psi_det_generators(:,:,:N_det_generators)
  psi_coef_sorted_gen(:N_det_generators, :) = psi_coef_generators(:N_det_generators, :)
  do i=1,inongen
    psi_det_sorted_gen_order(nongen(i)) = N_det_generators+i
    psi_det_sorted_gen(:,:,N_det_generators+i) = psi_det_sorted(:,:,nongen(i))
    psi_coef_sorted_gen(N_det_generators+i, :) = psi_coef_sorted(nongen(i),:)
  end do

END_PROVIDER


