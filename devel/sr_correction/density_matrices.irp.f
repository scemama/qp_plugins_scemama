BEGIN_PROVIDER [ double precision, two_e_dm_mo_singlet, (mo_num, mo_num, mo_num, mo_num, N_states) ]
 implicit none
 BEGIN_DOC
 ! Ps(r1,r2,r1',r2') = 1/2 (P(r1,r2,r1',r2') + P(r1,r2,r2',r1'))
 END_DOC
 integer :: i,j,k,l,istate

 do istate = 1,N_states
  do l=1,mo_num
   do k=1,mo_num
    do j=1,mo_num
     do i=1,mo_num
      two_e_dm_mo_singlet(i,j,k,l,istate) = 0.5d0 * (full_occ_2_rdm_spin_trace_mo(i,j,k,l,istate) + full_occ_2_rdm_spin_trace_mo(i,j,l,k,istate))
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER

BEGIN_PROVIDER [ double precision, two_e_dm_mo_triplet, (mo_num, mo_num, mo_num, mo_num, N_states) ]
 implicit none
 BEGIN_DOC
 ! Ps(r1,r2,r1',r2') = 1/2 (P(r1,r2,r1',r2') - P(r1,r2,r2',r1'))
 END_DOC
 integer :: i,j,k,l, istate

 do istate = 1,N_states
  do l=1,mo_num
   do k=1,mo_num
    do j=1,mo_num
     do i=1,mo_num
      two_e_dm_mo_triplet(i,j,k,l,istate) = 0.5d0 * (full_occ_2_rdm_spin_trace_mo(i,j,k,l,istate) - full_occ_2_rdm_spin_trace_mo(i,j,l,k,istate))
     enddo
    enddo
   enddo
  enddo
 enddo
END_PROVIDER

