program sr_correction_opt_psi
 implicit none
 read_wf = .True.
 use_only_lr = .False.
 SOFT_TOUCH read_wf use_only_lr
 call run
end

subroutine run
 implicit none
 integer :: istate
 PROVIDE all_mo_integrals
 do istate=1,N_states
 print *, '', istate
 print *, 'State ', istate
 print *, '---'
 print *, 'mu', mu_erf
 print *, 'E(mu)', energy_mu(istate)
 print *, '---'
 print *, 'W_bar(mu)', correction_mu(istate)
 print *, 'E(mu) + <W_bar(mu)>', energy_mu(istate) + correction_mu(istate)
 print *, '---'
 print *, 'alpha_0', alpha_coef(0)
 print *, 'correction 0', correction_alpha_0(istate)
 print *, 'E(mu) + alpha_0 <W_bar(mu)> = ', energy_mu(istate) + correction_alpha_0(istate)
 print *, '---'
 print *, 'alpha_1', alpha_coef(1)
 print *, 'correction 1', correction_alpha_1(istate)
 print *, 'E(mu) + alpha_0 <W_bar(mu)>_s + alpha_1 <W_bar(mu)>_t = ', energy_mu(istate) + correction_alpha_1(istate)
 print *, '---'
 print *, 'alpha_0_r', alpha_coef_r(0,istate)
 print *, 'correction 0', correction_alpha_0_r(istate)
 print *, 'E(mu) + alpha_0_r <W_bar(mu)> = ', energy_mu(istate) + correction_alpha_0_r(istate)
 print *, '---'
 print *, 'alpha_1_r', alpha_coef_r(1,istate)
 print *, 'correction 1', correction_alpha_1_r(istate)
 print *, 'E(mu) + alpha_0_r <W_bar(mu)>_s + alpha_1_r <W_bar(mu)>_t = ', energy_mu(istate) + correction_alpha_1_r(istate)
 print *, '---'


 print *,''
 print '(''|'',A6,''|'',5(A25,''|''))', 'mu', 'E(mu)', 'E(mu) + <W>', 'E(mu) + a_0<W>', &
 'E(mu) + a_0<W>_s + a_1<W>_t'
 print '(''|'',F6.2,''|'',5(F25.15,''|''))', mu_erf, energy_mu(istate), energy_mu(istate) + correction_mu(istate), energy_mu(istate) + &
 correction_alpha_0(istate), energy_mu(istate) + correction_alpha_1(istate)
 enddo
end
