#!/usr/bin/env python

import numpy as np
import subprocess
import sys, os
import time
from datetime import datetime

QP_PATH=os.environ["QP_ROOT"]+"/external/ezfio/Python/"
sys.path.insert(0,QP_PATH)

from ezfio import ezfio

#____________________________________________________________________________________
#
def read_ci_dress_svd():

    Ci_dress_svd_val = np.zeros((n_svd))
    Ci_dress_svd_err = np.zeros((n_svd))

    beg_read = results.find('Ci_dress_svd : [ ') + len( 'Ci_dress_svd : [ ' )
    end_read = len(results)
    vect_buf = results[beg_read:end_read]
    vect_buf = vect_buf.split( '\n' )

    for iline in range(1, n_svd+1):
        line = vect_buf[iline].split()

        # check
        indc = int( line[0] )
        if( indc != iline ):
            print(' Error in reading Ci_dress_svd')
            print(' indc  = {}'.format(indc) )
            print(' iline = {}'.format(iline) )
            break

        else:
            Ci_dress_svd_val[indc-1] = float( line[2] )
            Ci_dress_svd_err[indc-1] = float( line[4] )
            # !!!
        # !!!
    # !!!

    return(Ci_dress_svd_val,Ci_dress_svd_err)
#____________________________________________________________________________________


#____________________________________________________________________________________
#
def SVD_to_C():

    d_svd = np.array(ezfio.get_spindeterminants_psi_svd_coefs())
    U_svd = np.array(ezfio.get_spindeterminants_psi_svd_alpha())
    V_svd = np.array(ezfio.get_spindeterminants_psi_svd_beta())

    d_svd = d_svd[0,:]
    U_svd = U_svd[0,:,:].T
    V_svd = V_svd[0,:,:].T

    C_mat_val = np.dot(U_svd,np.dot(np.diagflat(d_svd),V_svd.T)) 

    n_det = ezfio.get_spindeterminants_n_det()
    i_row = np.array(ezfio.get_spindeterminants_psi_coef_matrix_rows())
    j_col = np.array(ezfio.get_spindeterminants_psi_coef_matrix_columns())
    
    C_val = np.zeros((n_det))
    for k in range(n_det):
        i        = i_row[k] - 1
        j        = j_col[k] - 1
        C_val[k] = C_mat_val[i,j] 

    return(C_val)
#____________________________________________________________________________________




#____________________________________________________________________________________
#
def save_to_file(v_val, name_f):

    with open(name_f, 'a') as ff:
        for i in range(len(v_val)):
            ff.write(' {}     {}\n'.format(i, v_val[i]))
#____________________________________________________________________________________



if __name__ == '__main__':
    t0 = time.time()

    # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    #
    #EZFIO_file = sys.argv[1]

    #EZFIO_file = "/home/aammar/qp2/src/svdwf/cr2_work/cr2_1p6_compactSVD50_DMC_fast"
    EZFIO_file = "/home/aammar/qp2/src/svdwf/cr2_work/cr2_1p6_compactSVD50_DMC_fast_2"
    #EZFIO_file = "/home/aammar/qp2/src/svdwf/cr2_work/cr2_1p6_compactSVD50_it1_DMC_fast"

    #EZFIO_file = "/home/aammar/qp2/src/svdwf/cr2_work/cr2_1p6_TSVD50_DMC_fast"
    #EZFIO_file = "/home/aammar/qp2/src/svdwf/cr2_work/cr2_1p6_TSVD50_it1_DMC_fast"
    # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

    ezfio.set_file(EZFIO_file)

    n_svd = ezfio.get_spindeterminants_n_svd_coefs()

    print(" ")
    print(" Today's date:", datetime.now() )
    print(" EZFIO file = {}".format(EZFIO_file))
    print(" n_svd = {}\n".format(n_svd) )
    if(n_svd <= 0):
        print(" n_svd <=0")
        break

    C_val = SVD_to_C()
    print(C_val)
    #save_to_file(C_val, 'C_val.txt')

    # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    #
    #                       SAVE TO EZFIO
    #
    ezfio.set_spindeterminants_psi_coef_matrix_values( C_val )
    ezfio.set_spindeterminants_n_svd_coefs(-1)
    # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
    #

    print("end after {:.3f} minutes".format((time.time()-t0)/60.) )
    # !!!
# !!!
