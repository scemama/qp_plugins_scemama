program print_2e_integrals

  BEGIN_DOC
  ! script to print 2-electron Coulomb integrals
  END_DOC

  implicit none

  read_wf = .true.
  TOUCH read_wf

  call run()

end 



subroutine run()

  implicit none

  integer                    :: i, j, k, l
  double precision, external :: get_ao_two_e_integral

  print *, ' ao_num = ', ao_num

!  provide ao_two_e_integrals_in_map
!  print *, hf_energy

  ! Map of Atomic integrals
  ! i(r1) j(r2) 1/r12 k(r1) l(r2)
  ! https://quantum-package.readthedocs.io/en/master/modules/ao_two_e_ints.html
  open(unit=11, file='ao_2eCoulombIntegrals.txt', action='write')
    do i = 1, ao_num 
      do k = 1, ao_num 
        do j = 1, ao_num 
          do l = 1, ao_num
!            write(11, '(e15.8)') get_two_e_integral(i, j, k, l, ao_integrals_map)
            write(11, *) get_ao_two_e_integral(i, j, k, l, ao_integrals_map)
          enddo
        enddo
      enddo
    enddo
  close(11)

end


