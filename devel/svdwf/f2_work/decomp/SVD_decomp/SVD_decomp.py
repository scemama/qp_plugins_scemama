#!/usr/bin/env python

from ezfio import ezfio
import numpy as np
import sys 

import time
from datetime import datetime

import matplotlib.pyplot as plt
#import seaborn as sns
from matplotlib.colors import LogNorm


# ____________________________________________________________________________________________
#
def read_Hsvd():

    Hsvd   = np.zeros((n_det_alpha*n_det_beta,n_det_alpha*n_det_beta))
    n_zero = 0
    with open("fort.7070") as f_in:
    #with open("fort.7071") as f_in:
        for line in f_in:
            ll = line.split()

            i  = int( ll[0] ) - 1
            j  = int( ll[1] ) - 1
            ii = j + n_det_beta * i

            k  = int( ll[2] ) - 1
            l  = int( ll[3] ) - 1
            jj = l + n_det_beta * k

            Hsvd[ii,jj] = float(ll[4])
            if( abs(Hsvd[ii,jj]) <= 1e-12):
                n_zero += 1

    print(' nb of zero element in Hsvd = {}/{}'.format(
                   n_zero, n_det_alpha*n_det_beta*n_det_alpha*n_det_beta    ))
    print(' %  of zero element in Hsvd = {}\n'.format(
            100. * n_zero / (n_det_alpha*n_det_beta*n_det_alpha*n_det_beta) ))
    return(Hsvd)
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________


# ____________________________________________________________________________________________
#
def read_Hdet():

    Hdet   = np.zeros((n_ab,n_ab))

    n_zero = 0
    with open("hdet_file") as f_in:
        for line in f_in:
            ll = line.split()

            i  = int( ll[0] ) - 1
            j  = int( ll[1] ) - 1
            ii = j + n_det_beta * i

            k  = int( ll[2] ) - 1
            l  = int( ll[3] ) - 1
            jj = l + n_det_beta * k

            Hdet[ii,jj] = float(ll[4])

            if( abs(Hdet[ii,jj]) <= 1e-12):
                n_zero += 1

    print(' nb of zero element in Hdet = {}/{}'.format(n_zero, n_ab*n_ab))
    print(' %  of zero element in Hdet = {} \n'.format(100.*n_zero/(n_ab*n_ab)))

    return(Hdet)
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________




# ____________________________________________________________________________________________
#
def read_Tdet():

    lines = []
    with open("ij_T_kl.dat") as f_in:
        for line in f_in:
            lines.append( float(line) )

    Tdet = np.zeros( (n_det_alpha*n_det_beta,n_det_alpha*n_det_beta) )

    i_l    = 0
    n_zero = 0
    for i in range(n_det_alpha):
        for j in range(n_det_beta):
            ii = j + n_det_beta * i

            for k in range(n_det_alpha):
                for l in range(n_det_beta):
                    jj = l + n_det_beta * k

                    Tdet[ii,jj] = lines[i_l]
                    i_l += 1

                    if( abs(Tdet[ii,jj]) <= 1e-12):
                        n_zero += 1

    print(' nb of zero element in Tdet = {}/{}'.format(
                   n_zero, n_det_alpha*n_det_beta*n_det_alpha*n_det_beta    ))
    print(' %  of zero element in Tdet = {}\n'.format(
            100. * n_zero / (n_det_alpha*n_det_beta*n_det_alpha*n_det_beta) ))

    return(Tdet)
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________





# ____________________________________________________________________________________________
#
def read_Tsvd():

    lines = []
    with open("ab_T_gd_v0.dat") as f_in:
        for line in f_in:
            lines.append( float(line) )

    Tsvd = np.zeros( (n_det_alpha*n_det_beta,n_det_alpha*n_det_beta) )

    i_l    = 0
    n_zero = 0
    for i in range(n_det_alpha):
        for j in range(n_det_beta):
            ii = j + n_det_beta * i

            for k in range(n_det_alpha):
                for l in range(n_det_beta):
                    jj = l + n_det_beta * k

                    Tsvd[ii,jj] = lines[i_l]
                    i_l += 1

                    if( abs(Tsvd[ii,jj]) <= 1e-12):
                        n_zero += 1

    print(' nb of zero element in Tsvd = {}/{}'.format(
                   n_zero, n_det_alpha*n_det_beta*n_det_alpha*n_det_beta    ))
    print(' %  of zero element in Tsvd = {}\n'.format(
            100. * n_zero / (n_det_alpha*n_det_beta*n_det_alpha*n_det_beta) ))

    return(Tsvd)
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________



# ____________________________________________________________________________________________
#
def read_AO_2eCoulomb():

    lines = []
    with open(ao_file) as f_in:
        for line in f_in:
            lines.append( float(line) )

    J = np.zeros((ao_num2,ao_num2))

    i_l    = 0
    n_zero = 0
    for mu in range(ao_num):
        for nu in range(ao_num):
            ii = nu + ao_num * mu

            for sig in range(ao_num):
                for lam in range(ao_num):
                    jj = lam + ao_num * sig

                    J[ii,jj] = lines[i_l]
                    i_l += 1

                    if( abs(J[ii,jj]) <= 1e-12):
                        n_zero += 1

    print(' nb of zero element in J = {}/{}'.format(n_zero, ao_num2*ao_num2))
    print(' %  of zero element in J = {}\n'.format(100.*n_zero/(ao_num2*ao_num2)))

    return(J)
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________



# ____________________________________________________________________________________________
#
def check_symmetric(a, rtol=1e-05, atol=1e-08):
    if( np.allclose(a, a.T, rtol=rtol, atol=atol) ):
        return( True )
    else:
        print(' | a - aT | / | a | = {} %'.format(
              100. * np.linalg.norm(a-a.T) / np.linalg.norm(a) ))
        return( False )
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________



# ____________________________________________________________________________________________
#
def plot_mat(M, xstep, ystep, namefile):

    Mp = np.abs(M) + 1e-15
    ax = sns.heatmap(Mp, linewidths=0.0, rasterized=True, norm=LogNorm())

    # x axis
    ax.xaxis.tick_top()
    xgrid = np.arange(1, M.shape[1]+1, xstep-1)
    plt.xticks( np.arange(0, M.shape[1], xstep-1) + .5, xgrid )

    # y axis
    ygrid = np.arange(1, M.shape[0]+1, ystep-1)
    plt.yticks( np.arange(0, M.shape[0], ystep-1) + .5, ygrid )

    plt.savefig(namefile)

    plt.clf()
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________



# ____________________________________________________________________________________________
#
def save_SVs():

    with open('sing_val.txt', 'w') as ff:
        for i in range(s.shape[0]):
            ff.write("{:+e}\n".format(s[i]))
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________















# ____________________________________________________________________________________________
# ____________________________________________________________________________________________
#
if __name__=="__main__":

    t0 = time.time()

    print("")
    print(" date and time = {} \n".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))

    EZFIO_name = "/home/aammar/qp2/src/svdwf/f2_work/f2_fci"
    ezfio.set_file(EZFIO_name)


    # --------------------    read M        ------------------------------

    n_det_alpha = ezfio.get_spindeterminants_n_det_alpha()
    n_det_beta  = ezfio.get_spindeterminants_n_det_beta ()
    n_ab        = n_det_alpha * n_det_beta 
    print(" n_det_alpha = {}   ".format(n_det_alpha))
    print(" n_det_beta  = {} \n".format(n_det_beta ))

    # < i j | H | k l > in det basis 
    hdet_file = "/home/aammar/qp2/src/svdwf/f2_work/fort.7000"
    M = read_Hdet()

    #M = read_Hsvd()
    #M = read_Tdet()
    #M = read_Tsvd()


    #ao_num  = ezfio.get_ao_basis_ao_num()
    #ao_num2 = ao_num * ao_num
    #print(" nb of AO = {} \n".format(ao_num))

    # 2-e integrals in AO basis 
    #ao_file = "/home/aammar/qp2/src/svdwf/f2_work/ao_2eCoulombIntegrals.txt"
    #M = read_AO_2eCoulomb()
 
    #plot_mat(M, 50, 50, 'Hdet.pdf') 
    #plot_mat(M, 50, 50, 'Hsvd.pdf') 
    #plot_mat(M, 50, 50, 'Tdet.pdf') 
    #plot_mat(M, 50, 50, 'Tsvd.pdf')
    # --------------------------------------------------------------------
    # --------------------------------------------------------------------



    # --------------------    perform SVD   ------------------------------

    norm_M = np.linalg.norm(M)
    print(" size of M: {} MB ".format( sys.getsizeof(M) / 1024. ) )
    print(" M is symmetric ? {} \n".format(check_symmetric(M)) )

    u, s, vt = np.linalg.svd(M, full_matrices=False)

    n_FSVD = s.shape[0] 
    err_p  = 100. * np.linalg.norm( 
             M - np.dot( u, np.dot(np.diag(s),vt) ) 
             ) / norm_M
    print(' n_FSVD = {}, err(%) = {}\n'.format(n_FSVD,err_p))
    # --------------------------------------------------------------------
    # --------------------------------------------------------------------
     
    save_SVs()


    # --------------------     T SVD         -----------------------------
    # truncatad SVD

    print(' n_FSVD        err(%)')
    for n_TSVD in range(1,n_FSVD):

        u_T, s_T, vt_T = u[:,0:n_TSVD], s[0:n_TSVD], vt[0:n_TSVD,:] 
  
        err_p  = 100. * np.linalg.norm( 
                 M - np.dot( u_T, np.dot(np.diag(s_T),vt_T) ) 
                 ) / norm_M

        print(' {}   {:+e}'.format(n_TSVD,err_p))

    # --------------------------------------------------------------------
    # --------------------------------------------------------------------



    print("")
    print(" end after {} min".format((-t0+time.time())/60.))
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________
# ____________________________________________________________________________________________

