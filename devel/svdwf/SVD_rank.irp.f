
program SVD_rank

  BEGIN_DOC
 ! rank SVD
  END_DOC

  implicit none

  read_wf = .True.
  TOUCH read_wf

  call run()

end


subroutine run

  implicit none

  integer                       :: mm, nn, mn, i_state, low_r
  double precision, allocatable :: Uezf_FSVD(:,:,:), Dezf_FSVD(:,:), Vezf_FSVD(:,:,:)
  double precision, allocatable :: Uezf_TSVD(:,:,:), Dezf_TSVD(:,:), Vezf_TSVD(:,:,:)
 
  i_state = 1
  mm      = n_det_alpha_unique
  nn      = n_det_beta_unique
  mn      = min(mm,nn)


  ! read TSVD
  allocate( Uezf_FSVD(mm,mm,1), Dezf_FSVD(mn,1), Vezf_FSVD(nn,nn,1) )
  call ezfio_get_spindeterminants_psi_svd_alpha(Uezf_FSVD)
  call ezfio_get_spindeterminants_psi_svd_beta (Vezf_FSVD)
  call ezfio_get_spindeterminants_psi_svd_coefs(Dezf_FSVD)

  ! new TSVD
  low_r = 100
  call ezfio_set_spindeterminants_n_svd_coefs(low_r)

  Dezf_TSVD(     1:low_r,1) = Dezf_FSVD(     1:low_r,1)
  Uezf_TSVD(1:mm,1:low_r,1) = Uezf_FSVD(1:mm,1:low_r,1)
  Vezf_TSVD(1:nn,1:low_r,1) = Vezf_FSVD(1:nn,1:low_r,1)
  deallocate( Uezf_FSVD, Dezf_FSVD, Vezf_FSVD )

  call ezfio_set_spindeterminants_psi_svd_alpha(Uezf_TSVD)
  call ezfio_set_spindeterminants_psi_svd_beta (Vezf_TSVD)
  call ezfio_set_spindeterminants_psi_svd_coefs(Dezf_TSVD)
  deallocate( Uezf_TSVD, Dezf_TSVD, Vezf_TSVD )



  !call ezfio_set_spindeterminants_n_det(N_det)
  !call ezfio_set_spindeterminants_n_states(N_states)
  !call ezfio_set_spindeterminants_n_det_alpha(n_det_alpha_unique)
  !call ezfio_set_spindeterminants_n_det_beta(n_det_beta_unique)
  !call ezfio_set_spindeterminants_psi_coef_matrix_rows(psi_bilinear_matrix_rows)
  !call ezfio_set_spindeterminants_psi_coef_matrix_columns(psi_bilinear_matrix_columns)
  !call ezfio_set_spindeterminants_psi_coef_matrix_values(psi_bilinear_matrix_values)

end
