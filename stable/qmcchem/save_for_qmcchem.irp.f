program save_for_qmc
  implicit none
  BEGIN_DOC
! Export data for QMC=Chem
  END_DOC
  integer                        :: iunit
  logical                        :: exists
  double precision               :: e_ref

  ! Determinants
  read_wf = .True.
  TOUCH read_wf
  print *,  "N_det = ", N_det
  call write_spindeterminants

  ! Reference Energy
  if (do_pseudo) then
    call write_pseudopotential
  endif
  call system( &
   'mkdir -p '//trim(ezfio_filename)//'/properties ;' // &
   'cp '//trim(ezfio_filename)//'/.version '//trim(ezfio_filename)//'/properties/.version ; ' // &
   'echo T > '//trim(ezfio_filename)//'/properties/e_loc' &
  )

  call ezfio_has_fci_energy_pt2(exists)
  if (exists) then
    call ezfio_get_fci_energy_pt2(e_ref)
  else
    call ezfio_has_fci_energy(exists)
    if (exists) then
      call ezfio_get_fci_energy(e_ref)
    else
      call ezfio_has_hartree_fock_energy(exists)
      if (exists) then
        call ezfio_get_hartree_fock_energy(e_ref)
      else
        e_ref = 0.d0
      endif
    endif
  endif
  call ezfio_set_simulation_e_ref(e_ref)

  if (do_frozen_core) then
    call ezfio_set_pseudo_core_fock_operator(core_fock_operator)
    call ezfio_set_simulation_do_frozen_core(.True.)
  endif

!  call ezfio_set_qmcchem_proj_potential_num(proj_num)
!  call ezfio_set_qmcchem_proj_potential(proj_potential)
  call export_trexio(.True., .True.)
end

! ---
