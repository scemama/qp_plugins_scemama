program print_basis
 implicit none
 BEGIN_DOC
! Truncate the wave function
 END_DOC
 integer :: i,j
 do i=1,ao_num
  do j=1,ao_prim_num(i)
    print *, i, j, ao_coef_normalized(i,j), ao_expo(i,j)
  enddo
 enddo
end
