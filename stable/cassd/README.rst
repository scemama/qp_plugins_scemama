=====
cassd
=====


|CIPSI| algorithm in the multi-reference CI space (CAS + Singles and Doubles).


This module is the same as the :ref:`fci` module, except for the choice of the
generator and selector determinants.

The inactive, active and virtual |MOs| will need to be set with the
:ref:`qp_set_mo_class` program.

.. seealso::

    The documentation of the :ref:`fci` module.


