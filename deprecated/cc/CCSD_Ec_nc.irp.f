BEGIN_PROVIDER [ double precision, EcCCSD ]
 implicit none
 BEGIN_DOC
 ! CCSD correlation energy in non-canonical form
 END_DOC

! Local variables

  integer                       :: i,j,a,b

  EcCCSD = 0d0

! Singles contribution

  do i=1,spin_occ_num
      do a=1,spin_vir_num
     
      EcCCSD = EcCCSD + spin_fock_matrix_mo_ov(i,a)*t1_cc(i,a)

    end do
  end do

! Doubles contribution

  do i=1,spin_occ_num
    do j=1,spin_occ_num
      do a=1,spin_vir_num
        do b=1,spin_vir_num
     
          EcCCSD = EcCCSD                              & 
                 + 0.5d0*OOVV(i,j,a,b)*t1_cc(i,a)*t1_cc(j,b) &
                 + 0.25d0*OOVV(i,j,a,b)*t2_cc(i,j,a,b)

        end do
      end do
    end do
  end do

END_PROVIDER

