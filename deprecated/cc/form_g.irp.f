subroutine form_g(hvv,hoo,t1,gvv,goo)

! Scuseria Eqs. (9), (10)

  implicit none

! Input variables

  double precision,intent(in)   :: hvv(spin_vir_num,spin_vir_num)
  double precision,intent(in)   :: hoo(spin_occ_num,spin_occ_num)

  double precision,intent(in)   :: t1(spin_occ_num,spin_vir_num)

! Local variables

  integer                       :: i,j,k,l
  integer                       :: a,b,c,d

! Output variables

  double precision,intent(out)  :: gvv(spin_vir_num,spin_vir_num)
  double precision,intent(out)  :: goo(spin_occ_num,spin_occ_num)

  gvv(:,:) = hvv(:,:)

  do a=1,spin_vir_num 
    do c=1,spin_vir_num
      do d=1,spin_vir_num
        do k=1,spin_occ_num
          gvv(c,a) = gvv(c,a) + VOVV(a,k,c,d)*t1(k,d)
        end do
      end do
    end do
  end do

  goo(:,:) = hoo(:,:)

  do k=1,spin_occ_num
    do i=1,spin_occ_num
      do c=1,spin_vir_num
        do l=1,spin_occ_num
          goo(i,k) = goo(i,k) + OOOV(k,l,i,c)*t1(l,c)
        end do
      end do
    end do
  end do

end subroutine form_g
