subroutine form_ub(ub)

! Form 1st term in (T) correction

  implicit none

! Local variables

  integer                       :: i,j,k,l
  integer                       :: a,b,c,d

! Output variables

  double precision,intent(out)  :: ub(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num)

  do c=1,spin_vir_num
    do b=1,spin_vir_num
      do a=1,spin_vir_num
        do k=1,spin_occ_num
          do j=1,spin_occ_num
            do i=1,spin_occ_num

              ub(i,j,k,a,b,c) = t1_cc(i,a)*OOVV(j,k,b,c) &
                              + t1_cc(i,b)*OOVV(j,k,c,a) &
                              + t1_cc(i,c)*OOVV(j,k,a,b) &
                              + t1_cc(j,a)*OOVV(k,i,b,c) & 
                              + t1_cc(j,b)*OOVV(k,i,c,a) &
                              + t1_cc(j,c)*OOVV(k,i,a,b) & 
                              + t1_cc(k,a)*OOVV(i,j,b,c) &
                              + t1_cc(k,b)*OOVV(i,j,c,a) & 
                              + t1_cc(k,c)*OOVV(i,j,a,b)

            end do
          end do
        end do
      end do
    end do
  end do

end subroutine form_ub
